from db import db
from validate_schema import validate_json


class SocketModel(db.Model):
    __tablename__ = 'sockets'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80))
    description = db.Column(db.String(200))

    clients_id = db.Column(db.Integer, db.ForeignKey('clients.id'), nullable= True)
    devices = db.relationship('DeviceModel', backref='SocketModel', lazy=True)

    def __init__(self, name, description, clients_id):
        self.name = name
        self.description = description
        self.clients_id = clients_id

    def json(self):
        return {
            'id': self.id,
            'name': self.name,
            'description': self.description,
            'clients_id': self.clients_id
        }

    @classmethod
    def find_by_id(cls, id):
        return cls.query.filter_by(id=id).first()

    @classmethod
    def find_all(cls):
        return cls.query.all()

    def save(self):
        db.session.add(self)
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()


class SocketSchema:

    schema = {
        'type': 'object',
        'properties': {
            'id': {'type': 'number'},
            'name': {'type': 'string'},
            'description': {'type': 'string'},
            'clients_id': {'type': 'number'}
        },
        'required': ['name', 'description', 'clients_id']
    }

    @classmethod
    def validate(cls, json_data):
        return validate_json(json_data, cls.schema)