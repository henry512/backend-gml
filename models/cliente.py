from db import db
from validate_schema import validate_json


class ClientModel(db.Model):
    __tablename__ = 'clients'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80))
    nit = db.Column(db.String(20))

    sockets = db.relationship('SocketModel', backref='ClientModel', lazy=True)

    def __init__(self, name, nit):
        self.name = name
        self.nit = nit

    def json(self):
        return {
            'id': self.id,
            'name': self.name,
            'nit': self.nit,
            #'sockets': self.sockets
        }

    @classmethod
    def find_by_id(cls, id):
        return cls.query.filter_by(id=id).first()

    @classmethod
    def find_all(cls):
        return cls.query.all()

    def save(self):
        db.session.add(self)
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()


class ClientSchema:

    schema = {
        'type': 'object',
        'properties': {
            'id': {'type': 'number'},
            'name': {'type': 'string'},
            'nit': {'type': 'string'}
        },
        'required': ['name', 'nit']
    }

    @classmethod
    def validate(cls, json_data):
        return validate_json(json_data, cls.schema)
