from db import db
from validate_schema import validate_json


class DeviceModel(db.Model):
    __tablename__ = 'devices'

    id = db.Column(db.Integer, primary_key=True)
    device_id = db.Column(db.String(80))
    device_type = db.Column(db.String(80))
    device_description = db.Column(db.String(200))
    device_connection = db.Column(db.String(80))

    sockets_id = db.Column(db.Integer, db.ForeignKey('sockets.id'), nullable=True)

    def __init__(self, device_id, device_type, device_description, device_connection, sockets_id):
        self.device_id = device_id
        self.device_type = device_type
        self.device_description = device_description
        self.device_connection = device_connection
        self.sockets_id = sockets_id

    def json(self):
        return {
            'id': self.id,
            'device_id': self.device_id,
            'device_type': self.device_type,
            'device_description': self.device_description,
            'device_connection': self.device_connection,
        }

    @classmethod
    def find_by_id(cls, id):
        return cls.query.filter_by(id=id).first()

    @classmethod
    def find_all(cls):
        return cls.query.all()

    def save(self):
        db.session.add(self)
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()


class DeviceSchema:

    schema = {
        'type': 'object',
        'properties': {
            'id': {'type': 'number'},
            'device_id': {'type': 'string'},
            'device_type': {'type': 'string'},
            'device_description': {'type': 'string'},
            'device_connection': {'type': 'string'},
            'sockets_id': {'type': 'number'}
        },
        'required': ['device_id', 'device_type', 'device_description', 'device_connection', 'sockets_id']
    }

    @classmethod
    def validate(cls, json_data):
        return validate_json(json_data, cls.schema)