from jsonschema import Draft7Validator


def validate_json(json_value, schema):
    error_validate = []
    validate = Draft7Validator(schema)

    for error in sorted(validate.iter_errors(json_value), key=str):
        print(error.message)
        error_validate.append(error.message)

    return error_validate
