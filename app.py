from flask import Flask, jsonify, request
from db import db

from models.cliente import ClientModel, ClientSchema
from models.socket import SocketModel, SocketSchema
from models.device import DeviceModel, DeviceSchema

app = Flask(__name__)
db.init_app(app)

app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:root@localhost/prueba'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['PROPAGATE_EXCEPTIONS'] = True


@app.before_first_request
def create_tables():
    db.create_all()


#RESOURCES CLIENTS
@app.route('/api/clients', methods=['GET'])
def get_all_clients():
    clients = [item.json() for item in ClientModel.find_all()]
    if clients:
        return jsonify(clients), 200
    return jsonify(None), 204

@app.route('/api/clients/<int:id>', methods=['GET'])
def get_first_clients(id):
    client = ClientModel.find_by_id(id)
    if client:
        return jsonify(client.json()), 200
    return jsonify(None), 204

@app.route('/api/clients', methods=['POST'])
def post_clients():
    content = request.json
    schema = ClientSchema.validate(content)
    if schema: #valida si el schema es correcto
        return jsonify(schema), 400

    client = ClientModel(
        name=content['name'],
        nit=content['nit'])

    try:
        client.save()
    except:
        return jsonify(None),500

    return jsonify(client.json()), 201

@app.route('/api/clients/<int:id>', methods=['PUT'])
def put_clients(id):
    content = request.json
    client = ClientModel.find_by_id(id)

    if client:
        schema = ClientSchema.validate(content)
        if schema:  # valida si el schema es correcto
            return jsonify(schema), 400

        client.name = content['name']
        client.nit = content['nit']

        try:
            client.save()
        except:
            return jsonify(None), 500

        return jsonify(client.json()), 200
    else:
        return jsonify(None), 404


@app.route('/api/clients/<int:id>', methods=['DELETE'])
def delete_clients(id):
    client = ClientModel.find_by_id(id)

    if client:
        try:
            client.delete()
        except:
            return jsonify(None), 500
    else:
        return jsonify(None), 404

    return jsonify(None), 204








#RESOURCES SOCKETS
@app.route('/api/sockets', methods=['GET'])
def get_all_sockets():
    sockets = [item.json() for item in SocketModel.find_all()]
    if sockets:
        return jsonify(sockets), 200
    return jsonify(None), 204

@app.route('/api/sockets/<int:id>', methods=['GET'])
def get_first_sockets(id):
    socket = SocketModel.find_by_id(id)
    if socket:
        return jsonify(socket.json()), 200
    return jsonify(None), 204

@app.route('/api/sockets', methods=['POST'])
def post_sockets():
    content = request.json
    schema = SocketSchema.validate(content)
    if schema: #valida si el schema es correcto
        return jsonify(schema), 400

    socket = SocketModel(
        name=content['name'],
        description=content['description'],
        clients_id=content['clients_id'])

    try:
        socket.save()
    except:
        return jsonify(None),500

    return jsonify(socket.json()), 201

@app.route('/api/sockets/<int:id>', methods=['PUT'])
def put_sockets(id):
    content = request.json
    socket = SocketModel.find_by_id(id)

    if socket:
        schema = SocketSchema.validate(content)
        if schema:  # valida si el schema es correcto
            return jsonify(schema), 400

        socket.name = content['name']
        socket.description = content['description']
        socket.clients_id = content['clients_id']

        try:
            socket.save()
        except:
            return jsonify(None), 500

        return jsonify(socket.json()), 200
    else:
        return jsonify(None), 404


@app.route('/api/sockets/<int:id>', methods=['DELETE'])
def delete_sockets(id):
    socket = SocketModel.find_by_id(id)

    if socket:
        try:
            socket.delete()
        except:
            return jsonify(None), 500
    else:
        return jsonify(None), 404

    return jsonify(None), 204






#RESOURCES DEVICES
@app.route('/api/devices', methods=['GET'])
def get_all_devices():
    devices = [item.json() for item in DeviceModel.find_all()]
    if devices:
        return jsonify(devices), 200
    return jsonify(None), 204

@app.route('/api/devices/<int:id>', methods=['GET'])
def get_first_devices(id):
    device = DeviceModel.find_by_id(id)
    if device:
        return jsonify(device.json()), 200
    return jsonify(None), 204

@app.route('/api/devices', methods=['POST'])
def post_devices():
    content = request.json
    schema = DeviceSchema.validate(content)
    if schema: #valida si el schema es correcto
        return jsonify(schema), 400

    device = DeviceModel(
        device_id=content['device_id'],
        device_description=content['device_description'],
        device_connection=content['device_connection'],
        device_type=content['device_type'],
        sockets_id=content['sockets_id'])

    try:
        device.save()
    except:
        return jsonify(None),500

    return jsonify(device.json()), 201

@app.route('/api/devices/<int:id>', methods=['PUT'])
def put_devices(id):
    content = request.json
    device = DeviceModel.find_by_id(id)

    if device:
        schema = DeviceSchema.validate(content)
        if schema:  # valida si el schema es correcto
            return jsonify(schema), 400

        device.device_id = content['device_id']
        device.device_description = content['device_description']
        device.device_connection = content['device_connection']
        device.device_type = content['device_type']

        try:
            device.save()
        except:
            return jsonify(None), 500

        return jsonify(device.json()), 200
    else:
        return jsonify(None), 404


@app.route('/api/devices/<int:id>', methods=['DELETE'])
def delete_devices(id):
    device = DeviceModel.find_by_id(id)

    if device:
        try:
            device.delete()
        except:
            return jsonify(None), 500
    else:
        return jsonify(None), 404

    return jsonify(None), 204



@app.route('/test')
def get():
    return jsonify({'message': 'test api OK...'}), 200


@app.errorhandler(404)
def not_found():
    return jsonify({'message': 'error 404'}), 404

if __name__ == '__main__':
    app.run()
