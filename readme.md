#Descripcion del problema
Desarrollo de CRUD, para las entidades cliente,
puntos de medidas y medidores, en el cual se exponen
las operaciones necesarias para gestionar los clientes
y sus respectivos puntos de medidas asociados a medidores

#Descripcion del entorno de trabajo
Se realiza el proyecto en windows 10, con python 3.8,
PostgreSQL 12, PyCharm Professional 2020.1, Repositorio 
Bitbucket, Postman, Las versiones del framework y demas librerias
se encuentran registradas en el archivo requirements.txt

#Planteamiento del esquema
Me base en 3 entidades, (clients, sockets, devices), las
cuales tienen una relacion uno a muchos y registran datos
basicos, aproximados a la HU del proyecto BPA

###Esquema de entidades

Clients(id, name, nit)

Sockets(id, name, description, clients_id)

Devices(id, device_id, device_type, device_description, device_connection, sockets_id)

###Esquema de relaciones de entidades

Clients - One-to-Many - Sockets

Sockets - One-to-Many - Devices

#Enlace publico de la colleción del API en Postman
https://www.getpostman.com/collections/32ce350899f4b75f3296